# User Server



User server is a microservice to store user accounts and perform simple authentication. For now, this is basic generation of the session(random)

## Getting started

1. Install Consul

	Consul is the default registry/discovery for go-micro apps. It's however pluggable.
	[https://www.consul.io/intro/getting-started/install.html](https://www.consul.io/intro/getting-started/install.html)

2. Run Consul
	```
	$ consul agent -server -bootstrap-expect 1 -data-dir /tmp/consul
	```

3. Start a mysql database

4. Download and start the service

	```shell
	go get github.com/micro/user-srv
	user-srv --database_url="root:root@tcp(192.168.99.100:3306)/user"
	```

	OR as a docker container

	```shell
	docker run microhq/user-srv --database_url="root:root@tcp(192.168.99.100:3306)/user" --registry_address=YOUR_REGISTRY_ADDRESS
	```

## RPC API

More defintiion at the proto (file)[https://gitlab.contetto.io/contetto-micro/users-srv/blob/master/src/service/proto/account/account.proto]
```
service Account {
   // Create a new user. On this RPC call, it should send request to the
   // sending-email-srv for sending activation email
   rpc Create(User) returns (CreateResponse) {}

   // Read current user by id
   // In the case, if user not exist, return error
   rpc Read(ReadRequest) returns (ReadResponse) {}

   // Update properties at the user body
   rpc Update(UpdateRequest) returns (UpdateResponse) {}

   // Delete user by id. This request shouldn't return value
   rpc Delete(DeleteRequest) returns (DeleteResponse) {}

   // Search users for the parameters
   rpc Search(SearchRequest) returns (SearchResponse) {}

   // GetUserByEmail returns user object by email
   rpc GetUserByEmail(SearchRequest) returns (SearchResponseOne) {}

   // Main method for auth of the user
   rpc LoginNative(LoginRequest) returns (LoginResponse) {}

   // UpdateSession provides append additional parameters to the session
   // after this, session will be updated
   // example of parameters: companId, brandId
   rpc UpdateSession(UpdateSessionRequest) returns (UpdateSessionResponse) {}

   // Social login provides a login via social networks(twitter, fb, google+)
   rpc SocialLogin(SocialLoginRequest) returns (SocialLoginResponse) {}

   // Changing of the password
   rpc ResetPassword(ResetPasswordRequest) returns (ResetPasswordResponse) {}

   // Logout provides removing current session
   rpc Logout(LogoutRequest) returns (LogoutResponse) {}

   // ValidateUser doing validation by the session
   rpc ValidateUser(ReadSessionRequest) returns(ReadSessionResponse) {}

   // Getting user object by the session
   rpc GetUserBySession(ReadSessionRequest) returns(UserResponse) {}

   // VerifyUser provides a verification of teh user by the activation link at email
   rpc VerifyUser(VerifyRequest) returns (VerifyResponse) {}

   // StoreEndpint saves api endpoint calls by the user(experimental)
   rpc StoreEndpoint(StoreEndpointRequest) returns (StoreEndpointResponse) {}

   // GetEnppoints returns list of endpoints atatched to the user
   rpc GetEndpoints(GetEndpointsRequest) returns (GetEndpointsResponse) {}
}
```


## Restful API

### Create a new user. Required parameters - "email", "password". After this, new user will be created and et "email" will be sending email for confirmation of registration.
```
curl -H "Content-Type:application/json" -X POST -d '{"data":{"attributes": {"email": "zzz@mail.ru", "password":"123", "city":"foo", "username": "boom", "firstName":"fun", "lastName":"zoom"}}}' https://api.contetto.io/users/v1/users
```

### Login as a user. Required parameters - "email", "password". Aftre login as a user, will be return current user's session.
```
curl -H "Content-Type:application/json" -X POST -d '{"data":{"attributes": {"email": "zzz@mail.ru", "password":"123"}}}' https://api.contetto.io/users/v1/sessions
```

**Response**
```
{
   "data":{
      "type":"sessions",
      "id":"TdWy0oKyzXlSW4CURPxyl5yKumkD5VzgWK34ELQo1o0PcwOsf0SlhV1CiNMv7y0AdVVPXHuI94jwM4qPAPZ45HLu2kBVo3N0ayG9JCVmafVqOTz5YzpITVrI0A7aqlsa",
      "attributes":{
         "created":"1463044972",
         "expires":"1463649772"
      },
      "relationships":{
         "users":{
            "type":"users",
            "id":"57344aad79f07d4691000003"
         }
      }
   }
}
```

### Read user object by id
```
curl -H "X-Session:<Session>" -H "Content-Type:application/json" https://api.contetto.io/users/v1/users/57344aad79f07d4691000003
```

### Delete a session by id. Returns status 204 if removing of the user was complete
```
curl -H "X-Session:<Session>" -X DELETE https://api.contetto.io/users/v1/sessions/id
```

**Response**
No Response



### Verification of user
Verification of the users working via inner RPC calls

```
curl -H "Content-Type:application/json" -H "X-Session:<Session>" 'https://api.contetto.io/users/v1/sessions/<Session>'
```

**Response**
```
{  
   "data":{  
      "type":"sessions",
      "id":"FzGzY54ABQC0KTK5ixun3PCsUQfA5FaOzOlkTE7BzVvo1OhtHrYP57aCSTOnTjFLNTNGuLwGAFMsWDIvii3RXxqhEvWDkLOA7mcjrBooCIDDFmWOwdLd0Wlz6Ly24xyV",
      "attributes":{  
         "created":1465114392,
         "email":"tta@ssmail.ru",
         "expires":1465719192,
         "username":""
      },
      "relationships":{  
         "user":{  
            "links":{  
               "self":{  
                  "href":"https://api.contetto.io/users/v1/users/5753df145af4fc18e5110022"
               }
            },
            "data":{  
               "type":"user",
               "id":"5753df145af4fc18e5110022"
            }
         }
      }
   }
}
```

### Remove session for user

```
 curl -H "X-Session:<Session>" -X DELETE https://api.contetto.io/users/v1/sessions
```

**Response**
Status code 204. No response
