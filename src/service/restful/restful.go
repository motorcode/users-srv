package restful

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	//"github.com/gorilla/securecookie"
	"github.com/fatih/structs"
	"github.com/gorilla/mux"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/client"
	check "gitlab.contetto.io/contetto-dev/check-request"
	"gitlab.contetto.io/contetto-micro/jsonapiv2"
	"gitlab.com/motorcode/users-srv/src/service/db"
	"gitlab.com/motorcode/users-srv/src/service/proto/account"
	"gitlab.contetto.io/contetto-micro/utils"
	"gitlab.contetto.io/contetto-micro/health"
	"gopkg.in/mgo.v2/bson"

	"golang.org/x/net/context"
)

type User struct{}

var (
	cl account.AccountClient
	Micro micro.Service
)

const (
	x         = "cruft123"
	alphanum  = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	cypherKey = "hghjgYG#t7@3t6iyg%7h^$gghhtg&&*k"
)
// Data to recognize the API itself
const (
	ContentType = "application/json"
	Domain      = "https://api.contetto.io"
	ServiceName = "users"
	APIVersion  = "v1"
)

// swagger:route POST /users usersApi usersPostApi
//
// Create a new user
//
// Only 201 is valid response code. Return object with id and links to resources
//
//	   Schemes: https
//
//     Produces:
//     - application/json
//     - application/vnd.api+json
//
//     Responses:
//       201:createUserResp
//       400:errorResp
//       401:errorResp
func Create(w http.ResponseWriter, r *http.Request) {
	var reqi account.CreateRequestREST
	var err error
	w.Header().Set("Content-Type", "application/vnd.api+json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&reqi)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Printf("Error on decoding of input structure")
		return
	}

	if reqi.Data == nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Printf("Error on decoding of input structure")
		jsonapi.WriteBasicError(w, "400", "Error on decoding of input structure", "Check request item")
		return
	}

	if reqi.Data.Attributes == nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Printf("Error on decoding of input structure")
		jsonapi.WriteBasicError(w, "400", "Error on decoding of input structure", "Check request item")
		return
	}

	usr := reqi.Data.Attributes

	req := client.NewRequest("users", "Account.Create", usr)

	rsp := &account.CreateResponse{}
	err = client.Call(context.Background(), req, rsp)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Println(err.Error())
		jsonapi.WriteBasicError(w, "400", err.Error(), "Check request item")
		return
	}

	w.WriteHeader(http.StatusCreated)
	jsonapi.PostObject(w, jsonapi.ObjectDataSingle{
		Data: jsonapi.Data{
			Type: "users",
			ID:   rsp.Id,
			Attributes: map[string]interface{}{
				"email":     usr.Email,
				"username":  usr.Username,
				"firstName": usr.FirstName,
				"lastName":  usr.LastName,
				"phoneNumber":  usr.PhoneNumber,
				"about":        usr.About,
				"address":      usr.Address,
				"city":         usr.City,
				"country":      usr.Country,
				"postalCode":   usr.PostalCode,
				"state":        usr.State,
			},

			Relationships: map[string]jsonapi.Relations{},
		},
	})
}

// swagger:route POST /sessions sessionsApi loginPostApi
//
// Native login for the user
//
// Only 201 is valid response code. Return object with session id
//
//
//     Produces:
//     - application/json
//
//     Schemes: https
//
//
//     Responses:
//       201: loginResp
//       400: errorResp
//       401: errorResp
func LoginNative(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "X-API-KEY, Origin, X-Requested-With, X-Session, Content-Type, Accept, Access-Control-Request-Method")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE, PATCH")
	var req account.LoginRequestREST
	var err error

	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "Check request item")
		return
	}

	resp, err := cl.LoginNative(context.TODO(), req.Data.Attributes)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "")
		return
	}

	w.WriteHeader(http.StatusCreated)
	jsonapi.PostObject(w, jsonapi.ObjectDataSingle{
		Data: jsonapi.Data{
			Type: "sessions",
			ID:   resp.Session.SessionId,
			Attributes: map[string]interface{}{
				"created": fmt.Sprintf("%d", time.Now().Unix()),
				"expires": fmt.Sprintf("%d", time.Now().Add(time.Hour*24*7).Unix()),
			},

			Relationships: map[string]jsonapi.Relations{
				"users": {
					Links: map[string]jsonapi.Link{
						"self": jsonapi.Link{
							Href: Domain + "/users/v1/users/" + resp.Session.UserId,
						},
					},
					Data: jsonapi.DataResLinkage{
						Type: "users",
						ID:   resp.Session.UserId,
					},
				},
			},
		},
	})
}


// swagger:route DELETE /sessions sessionsApi usersDeleteSession
//
// Logout. Remove current user's session
//
// Only 204 is valid response code. No content
//
//     Schemes: https
//
//     Security:
//		 session
//
//     Responses:
//       204: noContent
//       400: errorResp
//       401: errorResp
func Logout(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "X-API-KEY, X-Session, Origin, X-Requested-With, X-Session, Content-Type, Accept, Access-Control-Request-Method")
	session := r.Header.Get("X-Session")
	_, err := cl.Logout(context.TODO(), &account.LogoutRequest{SessionId: session})
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "")
		return
	}

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "")
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// swagger:route GET /sessions/validate sessionsApi validateUserGet
//
// Validation of the session. TODO. Need to make it as RPC call and remove this method
//
// Only 200 is valid response code. No content
//
//
//     Produces:
//     - application/json
//
//     Schemes: https
//
//
//     Responses:
//       200: noContent
//       400: errorResp
//       401: errorResp
func Validate(w http.ResponseWriter, r *http.Request) {
	var req account.ReadSessionRequestREST
	var err error
	w.Header().Set("Access-Control-Allow-Origin", "*")
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "Check request item")
		return
	}

	if req.Data == nil || req.Data.Attributes == nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", "API request is not jsonapi compatible", "Check request item")
		return
	}

	_, err = cl.ValidateUser(context.TODO(), req.Data.Attributes)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "Error on validation of user")
		return
	}

	if err != nil {
		jsonapi.WriteBasicError(w, "400", err.Error(), "")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
}

// swagger:route GET /users/{id} usersApi getUserApi
//
// Getting user by id
//
// Only 200 is valid response code. Return user object
//
//
//     Produces:
//     - application/json
//
//     Schemes: https
//
//     Security:
//		 session
//
//     Responses:
//       200: userGetApi
//       400: errorResp
//       401: errorResp
func GetUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", "id parameter is not defined", "")
		return
	}

	req := account.ReadRequest{Id: id}
	resp, err := cl.Read(context.TODO(), &req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "Check request item")
		return
	}

	if err != nil {
		jsonapi.WriteBasicError(w, "400", err.Error(), "Check request item")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
	jsonapi.PostObject(w, jsonapi.ObjectDataSingle{
		Data: jsonapi.Data{
			Type: "users",
			ID:   id,
			Attributes: map[string]interface{}{
				"email":     resp.User.Email,
				"username":  resp.User.Username,
				"firstName": resp.User.FirstName,
				"lastName":  resp.User.LastName,
				"phoneNumber":  resp.User.PhoneNumber,
				"about":        resp.User.About,
				"address":      resp.User.Address,
				"city":         resp.User.City,
				"country":      resp.User.Country,
				"postalCode":   resp.User.PostalCode,
				"state":   resp.User.State,
			},

			Relationships: map[string]jsonapi.Relations{
				"company": {
					Links: map[string]jsonapi.Link{
						"self": jsonapi.Link{
							Href: Domain + "/users/v1/users/" + id + "/relationships/company",
						},
					},
					Data: jsonapi.DataResLinkage{
						Type: "companies",
						ID:   resp.User.CompanyId,
					},
				},

				"brand": {
					Links: map[string]jsonapi.Link{
						"self": jsonapi.Link{
							Href: "/users/v1/users/" + id + "/relationships/brand",
						},
					},
					Data: jsonapi.DataResLinkage{
						Type: "brands",
						ID:   resp.User.BrandId,
					},
				},

			},
		},
	})
}

// swagger:route PATCH /users/{id} usersApi updateUserApi
//
// Update user by id
//
// Only 200 is valid response code.
//
//
//     Produces:
//     - application/json
//
//     Schemes: https
//
//
//     Responses:
//       200: createUserResp
//       400: errorResp
//       401: errorResp
func Update(w http.ResponseWriter, r *http.Request) {
	//var req account.CreateRequestREST
	var err error
	w.Header().Set("Content-Type", ContentType)
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "X-API-KEY, Origin, X-Requested-With, X-Session, Content-Type, Accept, Access-Control-Request-Method")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE, PATCH")

	if _, err := check.IsAuthenticated(r); err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		jsonapi.WriteBasicError(w, "401", err.Error(), "Check authorization token")
		return
	}

	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", "id parameter is not defined", "Check request item")
		return
	}

	// Get user object by id
	usrObj, err := db.FindOne(bson.M{"id": id})
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "Error on reading user model from mongo")
		return
	}

	item := account.CreateRequestREST{Data: &account.DataUser{Attributes: &usrObj}}

	//Trying to decode request body
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&item)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "Error on unmarshaling data")
		return
	}
	//var resp account.UpdateNameResponse
	_, err = cl.Update(context.TODO(), &account.UpdateRequest{
		Id:   id,
		SessionId: r.Header.Get("X-Session"),
		User: item.Data.Attributes,
	})

	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		log.Println(err.Error())
		jsonapi.WriteBasicError(w, "401", err.Error(), "")
		return
	}

	w.WriteHeader(http.StatusOK)
	jsonapi.PostObject(w, jsonapi.ObjectDataSingle{
		Data: jsonapi.Data{
			Type: "users",
			ID:   id,
			Attributes: map[string]interface{}{
				"email":     item.Data.Attributes.Email,
				"phoneNumber":  item.Data.Attributes.PhoneNumber,
				"about":        item.Data.Attributes.About,
				"address":      item.Data.Attributes.Address,
				"city":         item.Data.Attributes.City,
				"country":      item.Data.Attributes.Country,
				"postalCode":   item.Data.Attributes.PostalCode,
				"state":   item.Data.Attributes.State,
			},

			Relationships: map[string]jsonapi.Relations{
				"company": {
					Links: map[string]jsonapi.Link{
						"self": jsonapi.Link{
							Href: Domain + "/companies/v1/companies/" + item.Data.Attributes.CompanyId,
						},
					},
					Data: jsonapi.DataResLinkage{
						Type: "companies",
						ID:   item.Data.Attributes.CompanyId,
					},
				},

				"brand": {
					Links: map[string]jsonapi.Link{
						"self": jsonapi.Link{
							Href: Domain + "/brands/v1/brands/" + item.Data.Attributes.BrandId,
						},
					},
					Data: jsonapi.DataResLinkage{
						Type: "brands",
						ID:   item.Data.Attributes.BrandId,
					},
				},

			},
		},
	})
}

// swagger:route DELETE /users/{id} usersApi deleteUserApi
//
// Delete user by id
//
// Only 204 is valid response code. No content
//
//     Schemes: https
//
//     Responses:
//       204: noContent
//       400: errorResp
//       401: errorResp
func Remove(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE, PATCH")
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", "id parameter is not defined", "Check request item")
		return
	}

	req := &account.DeleteRequest{Id: id}
	_, err := cl.Delete(context.TODO(), req)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		jsonapi.WriteBasicError(w, "400", err.Error(), "User might be not found")
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

// swagger:route PATCH /users/{id}/sessions sessionsApi resetPass
//
// Reset password
//
// Only 200 is valid response code. No content
//
//
//     Produces:
//     - application/json
//
//     Schemes: https
//
//
//     Responses:
//       200: noContent
//       400: errorResp
//       401: errorResp
func ResetPassword(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "X-API-KEY, Origin, X-Requested-With, X-Session, Content-Type, Accept, Access-Control-Request-Method")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE, PATCH")
	if _, err := check.IsAuthenticated(r); err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		jsonapi.WriteBasicError(w, "401", err.Error(), "for call this method, needs access token")
		return
	}

	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", "id parameter is not defined", "Check request item")
		return
	}

	session := r.Header.Get("X-Session")

	var req account.ResetPasswordRequestREST
	//res := &account.ResetPasswordResponse{}
	//Trying to decode request body
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "Error on unmarshaling data")
		return
	}

	reqs := &account.ResetPasswordRequest{SessionID: session, UserID: id, OldPassword: req.Data.Attributes.OldPassword, NewPassword: req.Data.Attributes.NewPassword}
	res, err := cl.ResetPassword(context.TODO(), reqs)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "session is not found")
		return
	}

	sessionID := random(128)

	// Save session
	sess := &account.Session{
		SessionId: sessionID,
		UserId:    id,
		Username:  res.Session.Username,
		Created:   time.Now().Unix(),
		Expires:   time.Now().Add(time.Hour * 24 * 7).Unix(),
	}

	res.Session = sess
	// Store sessions
	if err := db.CreateSession(sess); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "During create of new session, raises error")
		return
	}

	w.WriteHeader(http.StatusOK)
	jsonapi.PostObject(w, jsonapi.Object{
		Data: []jsonapi.Data{
			jsonapi.Data{
				Type: "sessions",
				ID:   sessionID,
				Attributes: map[string]interface{}{
					"created": fmt.Sprintf("%d", time.Now().Unix()),
					"expires": fmt.Sprintf("%d", time.Now().Add(time.Hour*24*7).Unix()),
				},
				Relationships: map[string]jsonapi.Relations{
					"users": {
						Links: map[string]jsonapi.Link{
							"self": jsonapi.Link{
								Href: Domain + "/users/v1/users/" + res.Session.Username,
							},
						},
						Data: jsonapi.DataResLinkage{
							Type: "users",
							ID:   res.Session.Username,
						},
					},
				},
			},
		},
	})

}

func Search(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "X-API-KEY, Origin, X-Requested-With, X-Session, Content-Type, Accept, Access-Control-Request-Method")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE, PATCH")
	req := account.SearchRequest{
		Email:    r.FormValue("email"),
		City:     r.FormValue("city"),
		Username: r.FormValue("username"),
		Country:  r.FormValue("country"),
		//Limit: r.FormValue("limit"),
		//Offset: r.FormValue("offset")
	}
	resp, err := cl.Search(context.TODO(), &req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "")
		return
	}

	if err != nil {
		jsonapi.WriteBasicError(w, "400", err.Error(), "")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	data := []jsonapi.Data{}
	for _, user := range resp.Users {
		data = append(data, jsonapi.Data{
			ID:         user.Id,
			Type:       "users",
			Attributes: structs.Map(user),
		})
	}
	//value := structs.Map(resp.User)
	w.WriteHeader(http.StatusOK)
	jsonapi.PostObject(w, jsonapi.Object{
		Links: map[string]jsonapi.Link{
			"self": jsonapi.Link{
				Href: "/users",
			},
		},

		Data: data,
	})
}

// swagger:route GET /verifications usersApi verificationUserApi
//
//
// Only 200 is valid response code. Verification user provides makes user as verified
// if token is valid
//
//
//     Produces:
//     - application/json
//
//     Schemes: https
//
//
//     Responses:
//       200: noContent
//       400: errorResp
//       401: errorResp
func VerifyUser(w http.ResponseWriter, r *http.Request) {
	token := r.FormValue("token")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	if token == "" {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", "Token paremeter is not defined", "")
		return
	}

	req := account.VerifyRequest{Token: token}
	_, err := cl.VerifyUser(context.TODO(), &req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "")
		return
	}

	w.WriteHeader(http.StatusOK)
}

// swagger:route GET /sessions/{id} usersApi userbySessionApi
//
//
// Only 200 is valid response code. Return session object by the session token
//
//
//     Produces:
//     - application/json
//
//     Schemes: https
//
//
//     Responses:
//       200: sessionObject
//       400: errorResp
//       401: errorResp
func GetUserByTheSession(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "X-API-KEY, Origin, X-Requested-With, X-Session, Content-Type, Accept, Access-Control-Request-Method")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE, PATCH")
	session := r.Header.Get("X-Session")

	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", "Session id parameter is not defined", "")
		return
	}

	if id != session {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", "Session id's from header and on request is not identical", "")
		return
	}
	req := account.ReadSessionRequest{SessionId: session}
	var err error
	resp, err := cl.ValidateUser(context.TODO(), &req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "")
		return
	}

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "")
		return
	}

	respUser, err := cl.GetUserBySession(context.TODO(), &req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "")
		return
	}

	w.WriteHeader(http.StatusOK)
	jsonapi.PostObject(w, jsonapi.ObjectDataSingle{
		Data: jsonapi.Data{
			Type: "sessions",
			ID:   session,
			Attributes: map[string]interface{}{
				"username": resp.Session.Username,
				"email":    respUser.User.Email,
				"created":  resp.Session.Created,
				"expires":  resp.Session.Expires,
			},

			Relationships: map[string]jsonapi.Relations{
				"user": {
					Links: map[string]jsonapi.Link{
						"self": jsonapi.Link{
							Href: Domain + "/users/v1/users/" + resp.Session.UserId,
						},
					},
					Data: jsonapi.DataResLinkage{
						Type: "user",
						ID:   resp.Session.UserId,
					},
				},
			},
		},
	})
}

func UpdateSession(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "X-API-KEY, Origin, X-Requested-With, X-Session, Content-Type, Accept, Access-Control-Request-Method")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE, PATCH")
	session := r.Header.Get("X-Session")

	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", "Session id parameter is not defined", "")
		return
	}

	if id != session {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", "Session id's from header and on request is not identical", "")
		return
	}

	var req account.UpdateSessionRequestREST
	var err error

	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "Check request item")
		return
	}

	relationships := req.Data.Relationships
	attributes := &account.UpdateSessionRequest{}

	// Checking if relationships object is not empty
	if relationships != nil {
		// Then, applying role id and user id to attributes
		// If companies object is not empty
		if relationships.Companies != nil && relationships.Companies.Data != nil {
			attributes.CompanyId = relationships.Companies.Data.Id
		}

		// Checking brand on relatioships
		if relationships.Brands != nil && relationships.Brands.Data != nil {
			attributes.BrandId = relationships.Brands.Data.Id
		}
	}

	attributes.SessionId = session
	_, err = cl.UpdateSession(context.TODO(), attributes)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", err.Error(), "Unable to update session")
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func StoreEndpoints(w http.ResponseWriter, r *http.Request) {
	var reqi account.StoreEndpointRequestREST
	var err error
	w.Header().Set("Content-Type", "application/vnd.api+json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "X-API-KEY, Origin, X-Requested-With, X-Session, Content-Type, Accept, Access-Control-Request-Method")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, DELETE, PATCH")
	decoder := json.NewDecoder(r.Body)
	err = decoder.Decode(&reqi)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Printf("Error on decoding of input structure")
		return
	}

	if reqi.Data == nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Printf("Error on decoding of input structure")
		jsonapi.WriteBasicError(w, "400", "Error on decoding of input structure", "Check request item")
		return
	}

	if reqi.Data.Attributes == nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Printf("Error on decoding of input structure")
		jsonapi.WriteBasicError(w, "400", "Error on decoding of input structure", "Check request item")
		return
	}

	usr := reqi.Data.Attributes

	req := client.NewRequest("users", "Account.StoreEndpoint", usr)

	rsp := &account.StoreEndpointResponse{}
	err = client.Call(context.Background(), req, rsp)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Println(err.Error())
		jsonapi.WriteBasicError(w, "400", err.Error(), "Check request item")
		return
	}

	w.WriteHeader(http.StatusCreated)
	jsonapi.PostObject(w, jsonapi.ObjectDataSingle{
		Data: jsonapi.Data{
			Type: "usersLogs",
			ID:   rsp.Id,
			Attributes: map[string]interface{}{

			},

			Relationships: map[string]jsonapi.Relations{},
		},
	})
}

func SearchEndpoints(w http.ResponseWriter, r *http.Request) {
	// Get log of teh srored screens for the user
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "X-API-KEY, Origin, X-Requested-With, X-Session, Content-Type, Accept, Access-Control-Request-Method")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, DELETE, PATCH")

	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		jsonapi.WriteBasicError(w, "400", "Session id parameter is not defined", "")
		return
	}

	req := client.NewRequest("users", "Account.GetEndpoints", &account.GetEndpointsRequest{UserId: id})

	rsp := &account.GetEndpointsResponse{}
	err := client.Call(context.Background(), req, rsp)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Println(err.Error())
		jsonapi.WriteBasicError(w, "400", err.Error(), "Check request item")
		return
	}

	data := []jsonapi.Data{}
	for _, role := range rsp.Logs {
		data = append(data, jsonapi.Data{
			ID:   role.Id,
			Type: "usersLogs",
			Attributes: map[string]interface{}{

			},

			Links: map[string]jsonapi.Link{
				"self": jsonapi.Link{
					Href: Domain + "/users/v1/endpoints/" + role.Id,
				},
			},

			Relationships: map[string]jsonapi.Relations{
				"user": {
					Links: map[string]jsonapi.Link{
						"self": jsonapi.Link{
							Href: Domain + "/users/v1/users/" + role.UserId,
						},
					},
					Data: jsonapi.DataResLinkage{
						Type: "users",
						ID:   role.UserId,
					},
				},
			},
		})
	}
	w.WriteHeader(http.StatusOK)
	jsonapi.PostObject(w, jsonapi.Object{
		Data: data,
	})
}

// Users response
// swagger:response createUserResp
type UserResp struct {
	// Response for the user
	// in: body
	Body struct {
		// Links defined links to resources
		Links []struct {
			Meta map[string]string `json:"meta"`
			Self string            `json:"self"`
		} `json:"links"`

		// Data provides type of response and unique ID
		Data struct {
			Type string `json:"type"`
			ID   string `json:"id"`
			// Note: validationToken uses for validate user. Not as a user session! This is just for test
			ValidationToken string `json:"validationToken"`
		} `json:"data"`

		// Attributes from request
		Attributes struct {
			// user's email
			Email string `json:"email"`
			// user's first name
			FistName string `json:"firstName"`
			LastName string `json:"lastName"`
			Username string `json:"username"`
		} `json:"attributes"`
	}
}

func cors(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "X-API-KEY, Origin, X-Session, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE, PATCH")
}

func random(i int) string {
	bytes := make([]byte, i)
	rand.Read(bytes)
	for i, b := range bytes {
		bytes[i] = alphanum[b%byte(len(alphanum))]
	}
	return string(bytes)
}

func Init() {
	cl = account.NewAccountClient("users", client.DefaultClient)
	router := mux.NewRouter()
	router.HandleFunc("/users/v1/sessions/validate", Validate).Methods("GET")
	router.HandleFunc("/users/v1/sessions/validate", cors).Methods("OPTIONS")
	router.HandleFunc("/users/v1/users", Create).Methods("POST")
	router.HandleFunc("/users/v1/users", cors).Methods("OPTIONS")
	router.HandleFunc("/users/v1/users", Search).Methods("GET")
	router.HandleFunc("/users/v1/users/{id}", GetUser).Methods("GET")
	router.HandleFunc("/users/v1/users/{id}", Update).Methods("PATCH")
	router.HandleFunc("/users/v1/users/{id}", cors).Methods("OPTIONS")
	router.HandleFunc("/users/v1/users/{id}", Remove).Methods("DELETE")
	router.HandleFunc("/users/v1/users", cors).Methods("OPTIONS")
	router.HandleFunc("/users/v1/sessions", LoginNative).Methods("POST")
	router.HandleFunc("/users/v1/sessions", cors).Methods("OPTIONS")
	router.HandleFunc("/users/v1/sessions", Logout).Methods("DELETE")
	router.HandleFunc("/users/v1/sessions/{id}", GetUserByTheSession).Methods("GET")
	router.HandleFunc("/users/v1/sessions/{id}", UpdateSession).Methods("PATCH")
	router.HandleFunc("/users/v1/sessions/{id}", cors).Methods("OPTIONS")
	router.HandleFunc("/users/v1/users/{id}/sessions", ResetPassword).Methods("PATCH")
	router.HandleFunc("/users/v1/users/{id}/sessions", cors).Methods("OPTIONS")
	router.HandleFunc("/users/v1/verifications", VerifyUser).Methods("GET")
	router.HandleFunc("/users/v1/verifications", cors).Methods("OPTIONS")
	router.HandleFunc("/users/v1/endpoints/{id}", SearchEndpoints).Methods("GET")
	router.HandleFunc("/users/v1/endpoints", StoreEndpoints).Methods("POST")
	router.HandleFunc("/users/v1/endpoints", cors).Methods("OPTIONS")
	router.HandleFunc("/users/v1/sessions", cors).Methods("OPTIONS")
	router.HandleFunc("/users/v1/swagger", utils.SwaggerHandler).Methods("GET")
	check := health.HealthCheck{Router: router, Service:Micro, DB: db.MongoUsers, ServiceMethod: "Account.Health"}
	check.Apply("/users/v1")


	err := http.ListenAndServe(os.Getenv("MICRO_MUX_ADDRESS"), router)
	if err != nil {
		log.Fatal(err)
	}
}
