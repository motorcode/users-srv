// Package restful Users' API.
//
// Users' API helps you to store users and interact with them with a HTTP2
// friendly API.
//
// Terms Of Service:
//
// Who knows?
//
//
//		Schemes: https
//		Host: api.contetto.io
//    BasePath: /users/v1
//    Version: 1.0.0
//    License: None http://example.com/
//    Contact: On Air Netlines <email@example.com> http://contetto.io
//
//    Consumes:
//    - application/vnd.api+json
//    - application/json
//
//		Produces:
//    - application/vnd.api+json
//    - application/json
//
//
// swagger:meta
package restful

// Standard error response
// swagger:response errorResp
type Error struct {
	// Error body
	// in:body
	Body struct {
		// Id of this particular occurrence of the problem.
		//
		// required: true
		ID string `json:"id,omitempty"`
		// HTTP Status code of response, as a string.
		//
		// required: true
		Status string `json:"status,omitempty"`
		// Application-specific status code.
		//
		// required: true
		Code string `json:"code,omitempty"`
		// Title of the error
		//
		// required: true
		Title string `json:"title,omitempty"`
		// Advanced details about error.Optional
		//
		// required: true
		Detail string `json:"detail,omitempty"`
		// Source of the error.Optional
		//
		// required: true
		Source map[string]string `json:"source,omitempty"`
		// Meta of the error. Optional
		//
		// required: true
		Meta map[string]string `json:"meta,omitempty"`
	}
}

// Users definition for swagger
// swagger:parameters usersPostApi
type UserBody struct {
	// Create a new user
	// in:body
	Body struct {
		Data struct {
			Type       string `json:"type"`
			Attributes struct {
				// User's email
				Email string `json:"email"`
				// User's username
				Username string `json:"username"`
				// Password
				Password    string `json:"password"`
				FirstName   string `json:"firstName"`
				LastName    string `json:"lastName"`
				City        string `json:"city"`
				Country     string `json:"country"`
				PhoneNumber string `json:"phoneNumber"`
				Address     string `json:"address"`
				About       string `json:"about"`
				PostalCode  string `json:"postalCode"`
				State       string `json:"state"`
			} `json:"attributes"`
		} `json:"data"`
	}
}

// Reponse user's object
// swagger:response userGetApi
type UserGetBody struct {
	// Get user
	// in:body
	Body struct {
		Data struct {
			// Type of response
			//
			// Required: true
			Type       string `json:"type"`
			// Id of the user
			//
			// Required: true
			Id         string `json:"id"`
			Attributes struct {
				// User's email
				//
				// Required: true
				Email string `json:"email"`
				// User's username
				Username string `json:"username"`
				// Password
				Password    string `json:"password"`
				FirstName   string `json:"firstName"`
				LastName    string `json:"lastName"`
				City        string `json:"city"`
				Country     string `json:"country"`
				PhoneNumber string `json:"phoneNumber"`
				Address     string `json:"address"`
				About       string `json:"about"`
				PostalCode  string `json:"postalCode"`
				State       string `json:"state"`
			} `json:"attributes"`
		} `json:"data"`
	}
}

// swagger:parameters getUserApi deleteUserApi
type UserBodyResp struct {
	// Unique id of the user
	//
	// in: path
	// required: true
	Id string `json:"id"`
}

// swagger:parameters verificationUserApi
type VerificationRequest struct {
	// Validation token
	//
	// in:query
	// required: true
	Token string `json:"id"`
}

// Users definition for swagger
// swagger:parameters loginPostApi
type LoginBody struct {
	// Native login
	// in:body
	Body struct {
		Data struct {
			Type       string `json:"type"`
			Attributes struct {
				// User's email
				Email string `json:"email"`
				// Password
				Password string `json:"password"`
			} `json:"attributes"`
		} `json:"data"`
	}
}

// Response from login
// swagger:response loginResp
type LoginResp struct {
	// Body for the login response
	// in:body
	Body struct {
		// Links defined links to resources
		Links []struct {
			Meta map[string]string `json:"meta"`
			Self string            `json:"self"`
		} `json:"links"`

		Data struct {
			Type string `json:"type"`
			// User's session id.
			ID string `json:"id"`
		} `json:"data"`

		// Attributes from request
		Attributes struct {
			// Time in the UNIX format, when session was created
			Created string `json:"created"`
			// Time in the UNIX format, when session will be expiried
			Expires string `json:"expires"`
		} `json:"attributes"`

		Relationships struct {
		} `json:"relationships"`
	}
}

// swagger:parameters updateUserApi
type UpdateUserBody struct {
	// Unique id of the user
	//
	// in:path
	// required:true
	Id string `json:"id"`
	// Body for updating of user
	// in:body
	Body struct {
		Data struct {
			Type       string `json:"type"`
			Attributes struct {
				// User's email
				Email string `json:"email"`
				// User's username
				Username string `json:"username"`
				// Password
				Password    string `json:"password"`
				FirstName   string `json:"firstName"`
				LastName    string `json:"lastName"`
				City        string `json:"city"`
				Country     string `json:"country"`
				PhoneNumber string `json:"phoneNumber"`
				Address     string `json:"address"`
				About       string `json:"about"`
				PostalCode  string `json:"postalCode"`
				State       string `json:"state"`
			} `json:"attributes"`
		} `json:"data"`
	}
}

// swagger:parameters validateUserGet
type ValidationReuest struct {
	// Session for validation
	//
	// in:body
	Body struct {
		Data struct {
			Attributes struct {
				SessionID  string `json:"sessionId"`
			}`json:"attributes"`
		}`json:"data"`
	}

}

// ResetPassword to user
// swagger:parameters resetPass
type ResetPasswordBody struct {
	// id of the user
	// in:path
	// required:true
	Id string `json:"id"`
	// Definition of the body for reset password
	//
	// in:body
	Body struct {
		Data struct {
			Type       string `json:"type"`
			Attributes struct {
				// Old user's password
				OldPassword string `json:"oldpassword"`
				// New user's password
				NewPassword string `json:"newpassword"`
			} `json:"attributes"`
		} `json:"data"`
	}
}

// Session object for the user
// swagger:response sessionObject
type SessionObject struct {
	// Definition of the body for reset password
	//
	// in:body
	Body struct {
		Data struct {
			Type       string `json:"type"`
			Id string `json:"id"`
			Attributes struct {
				// session id
				Id string `json:"id"`
				// user id
				UserId string `json:"userId"`
				// Username
				Username string `json:"username"`
				// Email of the user
				Email   string  `json:"email"`
				// Session created at the unix time
				Created int64  `json:"created"`
				// Session expires at the unix time
				Expires int64 `json:"expires"`
			} `json:"attributes"`
		} `json:"data"`
	}
}

// Reponse for 204 error
// swagger:response noContent
type NoContent struct {
}
