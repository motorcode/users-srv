FROM golang:1.6.0

ADD . /go/src/gitlab.com/motorcode/users-srv

WORKDIR /go/src/gitlab.com/motorcode/users-srv/src/service

RUN pwd; ls -la

RUN go get --insecure ./...

EXPOSE     8080 8081 

RUN go build -o ../bin/service .
ENTRYPOINT ["/go/src/gitlab.com/motorcode/users-srv/src/bin/service"]